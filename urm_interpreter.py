#script per creare un interprete urm

import os, sys, re

#variabili globali
registri = []
indice_istruzione = 0

#funzioni
def Z(n):
    if len(registri) < (n):
        l = len(registri)
        for i in range(n-l):
            registri.append(0)
    registri[n-1] = 0
    
def S(n):
    if len(registri) < (n):
        l = len(registri)
        for i in range(n-l):
            registri.append(0)
    registri[n-1] +=1
    
def T(n, m):
    if len(registri) < (n):
        l = len(registri)
        for i in range(n-l):
            registri.append(0)
    if len(registri) < (m):
        l = len(registri)
        for i in range(m-l):
            registri.append(0)
    registri[m-1] = registri[n-1]
    
def J(n, m, t):
    if len(registri) < (n):
        l = len(registri)
        for i in range(n-l):
            registri.append(0)
    if len(registri) < (m):
        l = len(registri)
        for i in range(m-l):
            registri.append(0)
    if registri[n-1] == registri[m-1]:
        global indice_istruzione
        indice_istruzione = t-2


#funzioni per il debug        
def Zd(n):
    if len(registri) < (n):
        l = len(registri)
        for i in range(n-l):
            registri.append(0)
    global registri
    print('Memoria prima di esecuzione:', registri)
    print('Istruzione da eseguire: Z(%d)'%(n-1))
    registri[n-1] = 0
    print('Memoria dopo esecuzione:', registri)
    
def Sd(n):
    if len(registri) < (n):
        l = len(registri)
        for i in range(n-l):
            registri.append(0)
    global registri
    print('Memoria prima di esecuzione:', registri)
    print('Istruzione da eseguire: S(%d)'%(n-1))
    registri[n-1] +=1
    print('Memoria dopo esecuzione:', registri)
    
def Td(n, m):
    if len(registri) < (n):
        l = len(registri)
        for i in range(n-l):
            registri.append(0)
    if len(registri) < (m):
        l = len(registri)
        for i in range(m-l):
            registri.append(0)
    global registri
    print('Memoria prima di esecuzione:', registri)
    print('Istruzione da eseguire: T(%d, %d)'%(n-1, m-1))
    registri[m-1] = registri[n-1]
    print('Memoria dopo esecuzione:', registri)
    
def Jd(n, m, t):
    if len(registri) < (n):
        l = len(registri)
        for i in range(n-l):
            registri.append(0)
    if len(registri) < (m):
        l = len(registri)
        for i in range(m-l):
            registri.append(0)
    global registri
    print('Memoria prima di esecuzione:', registri)
    print('Istruzione da eseguire: J(%d, %d, %d)'%(n-1, m-1, t))
    if registri[n-1] == registri[m-1]:
        global indice_istruzione
        indice_istruzione = t-2
    print('Memoria dopo esecuzione:', registri)


#funzione per eseguire un file di input
def esegui_file(diz_parametri):
    lista_istruzioni = open(diz_parametri['nome_file'], 'r').read().split('\n')
    global indice_istruzione, registri
    registri = diz_parametri['memoria']
    while indice_istruzione < len(lista_istruzioni) and lista_istruzioni[indice_istruzione] != '':
        if 'D' in diz_parametri['debug'].upper():
            #modalita di debug attivata, mostro lo stato attuale del calcolo e l'istruzione da eseguire
            print('memoria:', registri)
            print('Prossima istruzione (riga %3d):'%(indice_istruzione +1),lista_istruzioni[indice_istruzione] )
            if 'A' not in diz_parametri['debug'].upper():
                #modalita di debug non automatica
                input('Premere invio per proseguire:')
        eval(lista_istruzioni[indice_istruzione], globals())
        indice_istruzione += 1
    if 'D' in diz_parametri['debug'].upper():
            #modalita di debug attivata, mostro lo stato attuale del calcolo
            print('Esecuzione terminata')
            print('Memoria:', registri)
            if 'A' not in diz_parametri['debug'].upper():
                #modalita di debug non automatica
                input('Premere invio per proseguire:')
    #a questo punto ha terminato l'esecuzione del programma ritorno il primo registro
    if len(registri) >0:
        print(registri[0])
        return registri[0]
    else:
        #registri lista vuota
        print('Lista registri vuota, programma non valido')
        return None


#variabili globali


if __name__ == '__main__':
    #if len(sys.argv) > 2:
    #ho inserito anche i parametri per il debug
    parametri = ' '.join(sys.argv[1:])
    diz_parametri = {'nome_file': '', 'debug': '','memoria':[] }
    #cerco il parametro per il debug
    l = re.findall(r'(-[dD][aA]?)', parametri)
    if len(l)!=0:
        #c'e il debug
        diz_parametri['debug'] = l[0]
        #cerco parametro memoria con opzione -r
    l = re.findall('-[rR] *(\[[^]]*\])', parametri)
    if len(l)!=0:
        #c'e il debug
        diz_parametri['memoria'] = eval(l[0])
    #cerco il nome del file
    l = re.findall('([^ ]*.urm)', parametri)
    if len(l) >0:
        diz_parametri['nome_file'] = [0]
        #print(diz_parametri)
        esegui_file(diz_parametri)
    else:
        pass
