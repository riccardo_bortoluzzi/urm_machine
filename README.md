# Interprete per il linguaggio URM

Interprete in python per un programma URM.

## Istruzioni per l'uso

Per eseguire un programma bisogna creare un file di testo con estensione .urm in cui inserire le istruzioni (J, Z, S, T).

Salvarlo nella stessa cartella in cui è presente l'interprete e lanciare il comando
```
python3 urm_interpreter [option] file.urm
``` 

Verrà stampato a video il risultato presente nel primo registro al termine dell'esecuzione del programma.

## Opzioni:

### -d 
Modalità di debug. Permette di visualizzare la memoria e la successiva istruzione da eseguire ad ogni passaggio. Per procedere bisogna premere Invio.

### -dA 
Modalità di debug automatica. Mostra lo stato del calcolo e l'istruzione da eseguire ad ogni passaggio senza chiedere l'intervento dell'utente

### -r lista
Registri iniziali. Si passano i valori dei registri sotto forma di lista.

## Esempio

Lanciando il comando 

```
python3 urm_interpreter.py -dA -r [1,5] somma.urm
```
Verrà eseguita la somma tra 1 e 5 in modalità debug automatico stampando tutti i passaggi

## Modalità linea di comando
È prevista una modalità da linea di comando. 
Per utilizzarla, lanciare il comando
```
python3 -i urm_interpreter.py -r [1,5]
```

Si entrerà quindi all'interno di una ambiene di linea di comando di python.
Per modificare la memoria basta modificare la variabile globale 
```
registri
```

In questa modalità si possono anche utilizzare le funzioni Jd, Zd, Sd, Td, analoghe a quelle di base per una macchina URM con la differenza che prevedono al loro interno già la modalità di debug incorporata.
